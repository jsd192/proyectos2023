from tkinter import messagebox
import datetime
import os
import pyautogui
import pyperclip
import mpv
import subprocess
import tkinter as tk
from mutagen.easyid3 import EasyID3

# Inicializar mpv
player = mpv.MPV()

# Crear una ventana Tkinter
ventana = tk.Tk()

# Crear una etiqueta para mostrar los metadatos
label = tk.Label(ventana, text="Metadatos de la canción")
label.pack()

# Función para actualizar la etiqueta con los metadatos de la canción
def update_metadata():
    # Obtener la información de metadatos de la canción actual
    if player.metadata:
       metadata = player.metadata.get("icy-title")
       if metadata:
           # Dividir la información de metadatos en artista y título
           artist, title = metadata.split(" - ")
           # Leer los metadatos de la canción desde el archivo mp3
           audio = EasyID3(player.filename)
           # Actualizar la etiqueta con los metadatos de la canción
           label.config(text=f"Artista: {artist}\nTítulo: {title}\nÁlbum: {audio['album'][0]}\nAño: {audio['date'][0]}")

    # Programar la actualización de la etiqueta cada segundo
    ventana.after(1000, update_metadata)

# Reproducir una canción
player.play("cancion.mp3")

# Llamar a la función para actualizar la etiqueta con los metadatos de la canción
update_metadata()

# Ejecutar la ventana Tkinter
ventana.mainloop()

