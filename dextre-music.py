import tkinter as tk
from tkinter import messagebox
import datetime
import os
import subprocess
import pyautogui
import pyperclip
import mpv
import signal

username = os.getenv('USER')
videos_dir = f"/home/{username}/Vídeos"
musica_dir = f"/home/{username}/Música"
reproductor = False
proceso = None
pyautogui.PAUSE = 5
video_descargado = False
audio_descargado = False
boton_anterior = None


def actualizar_hora():
    hora_actual = datetime.datetime.now().strftime("%H:%M:%S")
    label_hora.config(text=hora_actual)
    ventana.after(1000, actualizar_hora)

def descargar_video():
    global video_descargado
    url = url_entry.get()
    if not url:
        messagebox.showerror("ERROR", "Porfavor agrega una URL")
        return
    if not video_descargado:
        os.chdir(videos_dir)
        comando = f"yt-dlp {url}"
        os.system(comando)
        video_descargado = url
        messagebox.showinfo("Descarga completa", "El video descargado esta en: {}".format(videos_dir))
    elif video_descargado == url:
        messagebox.showinfo("ATENCIÓN!!", "no jodas, el video ya fue descargado")
    elif video_descargado != url:
        os.chdir(videos_dir)
        comando = f"yt-dlp {url}"
        os.system(comando)
        video_descargado = url 
        messagebox.showinfo("Descarga Completa", "El video descargado esta en: {}".format(videos_dir))
    else:
        messagebox.showinfo("ATENCIÓN!!", "El video ya ha sido descargado, no jodas.")
     
def clear_text():
    url_entry.delete(0, tk.END)

def reproducir_musica_blues():
    global reproductor, proceso, any_other_pressed
    url_blues = "https://streaming.live365.com/a36201"
    comando = ["mpv", url_blues]
    if  reproductor:
        proceso.send_signal(signal.SIGTERM)
        reproductor = False
        mensaje_audio.config(text="Detenido BLUES", bg='#FFCCCC') #rojo claro
    else:
        proceso = subprocess.Popen(comando)
        reproductor = True
        mensaje_audio.config(text="Se esta reproduciendo BLUES", bg='#C8E6C9') #verde claro
        
        
def reproducir_musica_rock():
    global reproductor, proceso, any_other_pressed
    url_rock = "https://listen.181fm.com/181-awesome80s_128k.mp3"
    comando = ["mpv", url_rock]
    if  reproductor:
        proceso.send_signal(signal.SIGTERM)
        reproductor = False
        mensaje_audio.config(text="Detenido ROCK, 80'S", bg='#FFCCCC') #rojo claro
    else:
        proceso = subprocess.Popen(comando)
        reproductor = True
        mensaje_audio.config(text="Se esta reproduciendo ROCK, 80'S",  bg='#C8E6C9') #verde claro
        
    
def reproducir_musica_salsa():
    global reproductor, proceso
    url_salsa = "https://server20.servistreaming.com:9023/stream"
    comando = ["mpv", url_salsa]
    if  reproductor:
        proceso.send_signal(signal.SIGTERM)
        reproductor = False
        mensaje_audio.config(text="Detenido SALSA",  bg='#FFCCCC') #rojo claro
    else:
        proceso = subprocess.Popen(comando)
        reproductor = True
        mensaje_audio.config(text="Se esta reproduciendo SALSA",  bg='#C8E6C9') #verde claro
        
   
def reproducir_musica_beatles():
    global reproductor, proceso, mensaje_audio, texto
    url_beatles = "http://listen.radionomy.com/beatlesradio"
    comando = ["mpv", url_beatles]
    if  reproductor:
        proceso.send_signal(signal.SIGTERM)
        reproductor = False
        mensaje_audio.config(text="Detenido THE BEATLES RADIO ONLINE", bg='#FFCCCC') #rojo claro
    else:
        proceso = subprocess.Popen(comando)
        reproductor = True
        mensaje_audio.config(text="Se esta reproduciendo THE BEATLES RADIO ONLINE", bg='#C8E6C9') #verde claro
        
def terminar():
    finish = os.system('salir3=$(pgrep mpv) && kill $salir3')

def cerrar_ventana():
    terminar()
    ventana.destroy()

    if proceso:
       proceso.kill()
       ventana.destroy()
       terminar()

def descargar_audio():
    global audio_descargado
    url2 = url_entry.get()
    if not url2:
        messagebox.showerror("ERROR", "Porfavor agrega una URL")
        return
    if not audio_descargado:
        os.chdir(musica_dir)
        comando = f"yt-dlp -x {url2}"
        os.system(comando)
        audio_descargado = url2
        messagebox.showinfo("Audio Completo", "El Audio descargado esta en: {}".format(musica_dir))
    elif audio_descargado == url2:
        messagebox.showinfo("ATENCIÓN!!", "no jodas, el Audio ya fue descargado")
    elif audio_descargado != url2:
        os.chdir(musica_dir)
        comando = f"yt-dlp -x {url2}"
        os.system(comando)
        audio_descargado = url2
        messagebox.showinfo("Descarga Completa", "El video descargado esta en: {}".format(musica_dir))
    else:
        messagebox.showinfo("ATENCIÓN!!", "El Audio ya ha sido descargado, no jodas.")


#tamaño predeterminado de la ventana
ventana = tk.Tk()
ventana.geometry("550x550+100+100")
ventana.protocol("WM_DELETE_WINDOW", cerrar_ventana)
ventana.title("Dextre Music")

def boton_detenido(boton):
    global proceso
    if proceso:
        boton["bg"] = "#D9F3D9"
    else:
        proceso is not None
        proceso.send_signal(signal.SIGTERM)
        proceso = None
        boton["bg"] = "#FFCCCC"

def change_button_color(boton):
    global reproductor

    if not reproductor:
        boton["bg"] = "#FFCCCC" #rojo claro
        reproductor = False
    else:
        # cambia de colo a rojo claro
        boton["bg"] = "#D9F3D9" #verde claro
        reproductor = True

def button_pressed(boton):
    global boton_anterior

    if boton_anterior is not None:
        boton_anterior["bg"] = original_colors[boton_anterior] #restaura el color del boton anterio

    change_button_color(boton)
    boton_anterior = boton #actualiza el boton anterior

def save_original_colors():
        original_colors[boton4] = boton4["bg"]
        original_colors[boton5] = boton5["bg"]
        original_colors[boton6] = boton6["bg"]
        original_colors[boton7] = boton7["bg"]


# Fuente grande para la hora
fuente_hora = ("Arial", 72, "bold")
label_hora = tk.Label(ventana, font=fuente_hora, text="")
label_hora.grid(row=0, column=0, columnspan=6, padx=8, pady=40)

#mostrando mensaje en un label
fuente_hora1 = ("roboto", 13, "bold")
label_mensaje = tk.Label(text="DESCARGA VIDEO/AUDIO", font=fuente_hora1)
label_mensaje.grid(row=1, column=0, columnspan=2, pady=5)

#radios online
fuente_hora1 = ("roboto", 13, "bold")
label_mensaje = tk.Label(text="RADIOS ONLINE", font=fuente_hora1)
label_mensaje.grid(row=1, column=2, columnspan=2, pady=5)

canvas = tk.Canvas(ventana, width=300, height=40)
canvas.grid(row=10, column=0, columnspan=6, pady=5, sticky='we')

texto = canvas.create_text(100, 25, text="La Mejor Música Del Mundo OnLine!!! Yeahh")

def mover_texto():
    # Obtener la posición actual del texto
    x, y = mensaje_audio.winfo_x(), mensaje_audio.winfo_y()
    x, y = canvas.coords(texto)

    # Mover el texto hacia la derecha
    x -= 1

    # Si el texto se ha movido fuera del borde derecho del Canvas,
    # moverlo de nuevo al borde izquierdo
    if x < -mensaje_audio.winfo_width():
        x = mensaje_audio.master.winfo_width()

    if x < -canvas.bbox(texto)[3]:
        x = canvas.winfo_width()

    # Actualizar la posición del texto en el Canvas
    mensaje_audio.place(x=x, y=1)
    canvas.coords(texto, x, y)

    # Llamar a esta función de nuevo después de 10 milisegundos
    ventana.after(35, mover_texto)

original_colors = {}  # Diccionario para almacenar los colores originales de los botones

#boton para salir
boton1 = tk.Button(ventana, text="Salir", command=cerrar_ventana, height=1, width=40)
boton1.grid(row=2, column=0, columnspan=2, pady=0)

url_entry = tk.Entry(ventana, fg='black', width=42)
url_entry.grid(row=3, column=0, columnspan=2, padx=15, pady=10)

mi_label = tk.Label(text="Ingrese la url del video para descargar")
mi_label.grid(row=4, column=0, columnspan=2,  padx=10, pady=10)

boton2 = tk.Button(ventana, text="Descargar Video", command=descargar_video, height=1, width=40)
boton2.grid(row=5, column=0, columnspan=2, pady=0)

#descargar audio
boton5 = tk.Button(ventana, text="Descargar el Audio del Video", command=descargar_audio, height=1, width=40)
boton5.grid(row=6, column=0, columnspan=2, pady=5)

#limpiar caja de entrada
boton3 = tk.Button(ventana, text="Limpiar Texto", command=clear_text, height=1, width=40)
boton3.grid(row=7, column=0, columnspan=2, pady=5)

#boton reproducir musica blues
boton4 = tk.Button(ventana, text="BLUES", command=lambda: (reproducir_musica_blues(), button_pressed(boton4)), width=25, height=1, bg="#FFFFFF")
#boton4 = tk.Button(ventana, text="BLUES", command=reproducir_musica_blues, width=25, height=1, bg="#FFFFFF")
boton4.grid(row=2, column=2, columnspan=2, pady=5)

#boton reproduce musica rock 80's
boton5 = tk.Button(ventana, text="ROCK", command=lambda: (reproducir_musica_rock(), button_pressed(boton5)), width=25, height=1, bg="#FFFFFF")
#boton5 = tk.Button(ventana, text="ROCK", command=reproducir_musica_rock, width=25, height=1, bg="#FFFFFF")
boton5.grid(row=3, column=2, columnspan=2, pady=5)

#boton reproducir musica salsa
boton6 = tk.Button(ventana, text="SALSA", command=lambda: (reproducir_musica_salsa(), button_pressed(boton6)), width=25, height=1, bg="#FFFFFF")
#boton6 = tk.Button(ventana, text="SALSA", command=reproducir_musica_salsa, width=25, height=1, bg="#FFFFFF")
boton6.grid(row=4, column=2, columnspan=2, pady=5)

boton7 = tk.Button(ventana, text="BEATLES RADIO ONLINE", command=lambda: (reproducir_musica_beatles(), button_pressed(boton7)), width=25, height=1, bg="#FFFFFF")
#boton7 = tk.Button(ventana, text="BEATLES RADIO ONLINE", command=reproducir_musica_beatles, width=25, height=1, bg="#FFFFFF")
boton7.grid(row=5, column=2, columnspan=2, pady=5)

mensaje_audio = tk.Label(text="")
mensaje_audio.grid(row=12, column=1, columnspan=1, pady=5, sticky='e')



clear_text()
actualizar_hora()
mover_texto()
save_original_colors() # Almacenar los colores originales antes de realizar cambios
ventana.mainloop()
