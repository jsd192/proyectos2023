#!/bin/bash

if [ "$(ls -l /home/$USER/Vídeos)" ]; then
    cantidad1=$(ls -l /home/$USER/Vídeos | wc -l)
    cantidad2=$(ls -l /home/$USER/Vídeos | wc -l)

    if [ $cantidad1 -eq $cantidad2 ] || [ $cantidad1 -lt $cantidad2 ]; then
        echo "Actualmente hay: $cantidad1 videos, antes había $cantidad2"
    else
        echo "Antes habia $cantidad1 ahora Hay $cantidad2, un video menos"
        if [ $cantidad2 -gt $cantidad1 ]; then
            echo "Sí hay un video más descargado, ahora hay $cantidad2"
        fi
    fi
else
    echo "No se encontraron videos en /home/$USER/Vídeos"
fi
