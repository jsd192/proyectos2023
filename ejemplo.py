import tkinter as tk
from tkinter import messagebox
import datetime
import os
import subprocess
import pyautogui
import pyperclip
import mpv
import signal

username = os.getenv('USER')
videos_dir = f"/home/{username}/Vídeos"
musica_dir = f"/home/{username}/Música"
reproductor = False
proceso = None
pyautogui.PAUSE = 5
video_descargado = False
audio_descargado = False


def actualizar_hora():
    hora_actual = datetime.datetime.now().strftime("%H:%M:%S")
    label_hora.config(text=hora_actual)
    ventana.after(1000, actualizar_hora)

def descargar_video():
    global video_descargado
    url = url_entry.get()
    if not url:
        messagebox.showerror("ERROR", "Porfavor agrega una URL")
        return
    if not video_descargado:
        os.chdir(videos_dir)
        comando = f"yt-dlp {url}"
        os.system(comando)
        video_descargado = url
        messagebox.showinfo("Descarga completa", "El video descargado esta en: {}".format(videos_dir))
    elif video_descargado == url:
        messagebox.showinfo("ATENCIÓN!!", "no jodas, el video ya fue descargado")
    elif video_descargado != url:
        os.chdir(videos_dir)
        comando = f"yt-dlp {url}"
        os.system(comando)
        video_descargado = url 
        messagebox.showinfo("Descarga Completa", "El video descargado esta en: {}".format(videos_dir))
    else:
        messagebox.showinfo("ATENCIÓN!!", "El video ya ha sido descargado, no jodas.")
     
def clear_text():
    url_entry.delete(0, tk.END)

def reproducir_musica_blues():
    global reproductor, proceso
    url_blues = "https://streaming.live365.com/a36201"
    comando = ["mpv", url_blues]
    if  reproductor:
        proceso.send_signal(signal.SIGTERM)
        reproductor = False
        mensaje_audio.config(text="Detenido PURITO BLUES")
    else:
        proceso = subprocess.Popen(comando)
        reproductor = True
        mensaje_audio.config(text="Se esta reproduciendo PURITO BLUES")

def reproducir_musica_rock():
    global reproductor, proceso, player
    url_rock = "https://listen.181fm.com/181-awesome80s_128k.mp3"
    comando = ["mpv", url_rock]
    if  reproductor:
        proceso.send_signal(signal.SIGTERM)
        #proceso.kill()
        reproductor = False
        mensaje_audio.config(text="Detenido ROCK, 80'S")
    else:
        proceso = subprocess.Popen(comando)
        reproductor = True
        mensaje_audio.config(text="Se esta reproduciendo ROCK, 80'S")
        mensaje_audio2.config()

def reproducir_musica_salsa():
    global reproductor, proceso
    url_salsa = "https://server20.servistreaming.com:9023/stream"
    comando = ["mpv", url_salsa]
    if  reproductor:
        proceso.send_signal(signal.SIGTERM)
        reproductor = False
        mensaje_audio.config(text="Detenido SALSA DURA")
    else:
        proceso = subprocess.Popen(comando)
        reproductor = True
        mensaje_audio.config(text="Se esta reproduciendo SALSA DURA")

def cerrar_ventana():
    global proceso
    if proceso:
        proceso.kill()
    ventana.destroy()

def descargar_audio():
    global audio_descargado
    url2 = url_entry.get()
    if not url2:
        messagebox.showerror("ERROR", "Porfavor agrega una URL")
        return
    if not audio_descargado:
        os.chdir(musica_dir)
        comando = f"yt-dlp -x {url2}"
        os.system(comando)
        audio_descargado = url2
        messagebox.showinfo("Audio Completo", "El Audio descargado esta en: {}".format(musica_dir))
    elif audio_descargado == url2:
        messagebox.showinfo("ATENCIÓN!!", "no jodas, el Audio ya fue descargado")
    elif audio_descargado != url2:
        os.chdir(musica_dir)
        comando = f"yt-dlp -x {url2}"
        os.system(comando)
        audio_descargado = url2
        messagebox.showinfo("Descarga Completa", "El video descargado esta en: {}".format(musica_dir))
    else:
        messagebox.showinfo("ATENCIÓN!!", "El Audio ya ha sido descargado, no jodas.")

#tamaño predeterminado de la ventana
ventana = tk.Tk()
ventana.geometry("400x650+100+100")

# Fuente grande para la hora
fuente_hora = ("Arial", 72, "bold")
label_hora = tk.Label(ventana, font=fuente_hora, text="")
label_hora.grid(row=0, column=0, columnspan=2, padx=10, pady=40)

#mostrando mensaje en un label
fuente_hora1 = ("roboto", 13, "bold")
label_mensaje = tk.Label(text="MOSTRANDO LA HORA ACTUAL", font=fuente_hora1)
label_mensaje.grid(row=1, column=0, columnspan=2, pady=5)

#boton para salir
boton1 = tk.Button(ventana, text="Salir", command=cerrar_ventana, height=1, width=40)
boton1.grid(row=2, column=0, columnspan=2, pady=0)

url_entry = tk.Entry(ventana, fg='black', width=42)
url_entry.grid(row=3, column=0, columnspan=2, padx=15, pady=10)

mi_label = tk.Label(text="Ingrese la url del video para descargar")
mi_label.grid(row=4, column=0, columnspan=2,  padx=10, pady=10)

boton2 = tk.Button(ventana, text="Descargar Video", command=descargar_video, height=1, width=40)
boton2.grid(row=5, column=0, columnspan=2, pady=0)

#descargar audio
boton5 = tk.Button(ventana, text="Descargar el Audio del Video", command=descargar_audio, height=1, width=40)
boton5.grid(row=6, column=0, columnspan=2, pady=5)

#limpiar caja de entrada
boton3 = tk.Button(ventana, text="Limpiar Texto", command=clear_text, height=1, width=40)
boton3.grid(row=7, column=0, columnspan=2, pady=5)

#boton reproducir musica blues
boton4 = tk.Button(ventana, text="BLUES", command=reproducir_musica_blues, height=1, width=40)
boton4.grid(row=8, column=0, columnspan=2, pady=5)

#boton reproduce musica rock 80's
boton5 = tk.Button(ventana, text="ROCK", command=reproducir_musica_rock, width=40, height=1)
boton5.grid(row=9, column=0, columnspan=2, pady=5)

#boton reproducir musica salsaSS
boton6 = tk.Button(ventana, text="SALSA", command=reproducir_musica_salsa, width=40, height=1)
boton6.grid(row=10, column=0, columnspan=2, pady=5)

mensaje_relleno = tk.Label(text="")
mensaje_relleno.grid(row=11)

mensaje_audio = tk.Label(text="")
mensaje_audio.grid(row=12, column=0, columnspan=1, pady=5, sticky='e')

mensaje_audio2 = tk.Label(text="")
mensaje_audio2.grid(row=13, column=0, columnspan=1, pady=5, sticky='e')

#update_song_info()
clear_text()
actualizar_hora()
ventana.mainloop()
