#!/bin/bash

# Define la ruta del directorio a monitorear
dir_path=/home/$USER/Vídeos

# Almacena el número inicial de archivos
#num_files=$(ls -l "$dir_path" | grep ^- | wc -l)
num_files=$(ls -l "$dir_path" | wc -l)

# Imprime el número inicial de archivos
echo "El directorio $dir_path tiene $num_files archivos."

# Ejecuta un ciclo infinito para monitorear los cambios en el directorio
while true; do
    # Cuenta el número actual de archivos
    #current_num_files=$(ls -l "$dir_path" | grep ^- | wc -l)
    current_num_files=$(ls -l "$dir_path" | wc -l)

    # Compara el número actual de archivos con el anterior
    if [[ $current_num_files -gt $num_files ]]; then
        # Si se agregó un archivo, actualiza el número de archivos y muestra un mensaje
        num_files=$current_num_files
        echo "Se agregó un nuevo archivo. El directorio $dir_path ahora tiene $num_files archivos."
    elif [[ $current_num_files -lt $num_files ]]; then
        # Si se eliminó un archivo, actualiza el número de archivos y muestra un mensaje
        num_files=$current_num_files
        echo "Se eliminó un archivo. El directorio $dir_path ahora tiene $num_files archivos."
    fi

    # Espera 10 segundos antes de volver a comprobar el número de archivos
    sleep 10
done